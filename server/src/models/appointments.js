const database = require('./database');

/**
 * Persistence layer for appointments
 * 
 * This class is responsible for mapping between the string type of the appointment id
 * and the internal integer representation in the database.
 */

/**
 * @returns { Appointment[] } Array of all appointments
 */
exports.getAll = async () => {
  const db = database.getDatabase();

  const rows = await db.all('SELECT id, patient_id, start_time, duration FROM appointments');
  return rows.map(row => {
    return {
      id: row.id,
      patientId: row.patient_id,
      startTime: row.start_time,
      duration: row.duration
    };
  });
};

/**
 * @param { string } id Appointment's unique ID
 * @returns { Appointment }
 */
exports.get = async (id) => {
  const db = database.getDatabase();

  const row = await db.get(
    'SELECT id, patient_id, start_time, duration FROM appointments WHERE id = $id',
    { $id: +id }
  );
  if (!row) {
    return null;
  }
  return {
    id: '' + row.id,
    patientId: '' + row.patient_id,
    startTime: row.start_time,
    duration: row.duration
  };
};

/**
 * Return all appointments whose start time is in the half-open range [start, end) where start
 * and end are both in UTC.
 * 
 * @param { Moment } start Start time in UTC
 * @param { Moment } end End time in UTC
 * @returns { Appointment[] } Array of all appointments starting between [start, end)
 */
exports.getBetween = async (start, end) => {
  const db = database.getDatabase();

  const rows = await db.all(
    'SELECT id, patient_id, start_time, duration FROM appointments WHERE start_time >= $start AND start_time < $end',
    { $start: start.utc().format(), $end: end.utc().format() }
  );
  return rows.map(row => {
    return {
      id: '' + row.id,
      patientId: '' + row.patient_id,
      startTime: row.start_time,
      duration: row.duration
    };
  });
};

/**
 * @param { string } id Appointment's unique ID
 */
exports.delete = async (id) => {
  const db = database.getDatabase();

  const row = await db.run('DELETE FROM appointments WHERE id = $id', +id);
  if (!row.changes) {
    throw new Error(`Failed to delete appointment id '${id}'`);
  }
};
