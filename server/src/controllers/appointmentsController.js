/**
 * Read and delete routes for appointments.  Create and update left as an exercise to the reader.
 */

const moment = require('moment-timezone');
const appointments = require('../models/appointments');

exports.appointments_list = async function (_, res) {
  const allAppointments = await appointments.getAll();
  res.status(200).json({appointments: allAppointments});
};

exports.appointments_showById = async function (req, res) {
  const appointment = await appointments.get(req.params.id);
  if (appointment) {
    return res.status(200).json(appointment);
  }
  res.status(404).send();
};

/**
 * Returns a list of all appointments occurring within a single day where
 * the start and end of the day are defined by the tz query parameter.  Defaults
 * to Etc/UTC where tz is not present.
 */
exports.appointments_showByDate = async function (req, res) {
  const tz = req.query.tz || 'Etc/UTC';
  if (!moment.tz.zone(tz)) {
    return res.status(400).send();
  }

  const date = moment.tz(req.params.date, 'YYYY-MM-DD', true, tz);
  if (!date.isValid()) {
    return res.status(404).send();
  }

  const appointmentsOnDay = await appointments.getBetween(date, date.clone().add(1, 'day'));
  return res.status(200).json({ appointments: appointmentsOnDay });
};

exports.appointments_delete = async function(req, res) {
  try {
    await appointments.delete(req.params.id);
    return res.status(204).send();
  } catch (err) {
    return res.status(404).send();
  }
};
