import { ScheduleItemComponent } from './schedule-item.component';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { MOMENT_TOKEN } from '../shared/moment.service';
import * as moment from 'moment-timezone';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { By } from '@angular/platform-browser';

describe('ScheduleItemComponent', () => {
  let fixture: ComponentFixture<ScheduleItemComponent>;
  let component: ScheduleItemComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleItemComponent ],
      imports: [ FontAwesomeModule ],
      providers: [
        { provide: MOMENT_TOKEN, useValue: moment }
      ]
    });

    fixture = TestBed.createComponent(ScheduleItemComponent);
    component = fixture.componentInstance;
  });

  it('can create the component', () => {
    expect(component).toBeTruthy();
  });

  it('displays start and end in local time', () => {
    moment.tz.setDefault('America/Los_Angeles');
    component.appointment = {
      id: '1',
      patientId: '2',
      startTime: '2018-06-14T17:10:04Z',
      duration: 30
    };
    fixture.detectChanges();

    expect(fixture.nativeElement.innerHTML).toContain('10:10 AM - 10:40 AM');
  });

  it('emits delete event when icon clicked', (done) => {
    component.delete.subscribe(appointment => {
      expect(appointment).toBe(appointment);
      done();
    });

    fixture.debugElement.query(By.css('fa-icon')).nativeElement.click();
  });
});
