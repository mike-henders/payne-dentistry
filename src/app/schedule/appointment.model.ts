export interface Appointment {
  id: string;
  patientId: string;
  startTime: string;
  duration: number;
}
